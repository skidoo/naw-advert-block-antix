advert-block-antix
==================

Blocks connections to advertising webservers via hosts file

This program presents a UI enabling the user to select from a curated set of blocklists.
The selected blocklists are retrieved and their entries are sorted, de-duplicated,
then merged into /etc/hosts 
On demand, the user may elect to "uninstall" all blocklists, and
advert-block-antix will revert /etc/hosts to its original state.

 (On antiX systems, this tool is provided via “advert-block-antix” debfile package.)

a video, showing an earlier version of this tool: https://www.youtube.com/watch?v=DPX8Zqyo8vk

=========

At the links below, you can view the contents of each of the blocklists
currently available for installation via advert-block-antix.
Also a "description" link is cited for each, in case you care to read
the list maintainer’s description / explanation of what a given list contains.

“mvps” list: http://winhelp2002.mvps.org/hosts.txt <br>
^—–} description is here http://winhelp2002.mvps.org <br>
(as of 8 Jan 2018: 13,017 entries; 372kb) (unavailable via http<b>s</b>)<br>

“someonwhocares” list: http://someonewhocares.org/hosts/hosts <br>
^—–} description is here http://someonewhocares.org/hosts/ <br>
(as of 8 Jan 2018: 13,170 entries; 388kb) (unavailable via http<b>s</b>)<br>

“yoyo” list: https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext <br>
^—–} description is here: http://pgl.yoyo.org/adservers/ <br>
(as of 8 Jan 2018: 2,520 entries; 68kb)<br>

“adservers hosts” list: h t t p s: / / hosts-file.net/ad_servers.asp <br>
not hyperlinked in this post b/c it is a looooooong list (if loaded into a web browser, the browser may stall/crash)<br>
^—–} description is here: http://hosts-file.net/ <br>
(as of 8 Jan 2018: 47,809 entries; 1.7MB)<br>


